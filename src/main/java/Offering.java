import java.sql.*;

public class Offering extends Base{
	private int id;
	private Course course;
	private String daysTimes;

	static String dbName="reggie";
	static String connectionURL = "jdbc:derby:" + dbName + ";create=true";


	public static Offering create(Course course, String daysTimesCsv) throws Exception {
		//Connection conn = null;

			//conn = DriverManager.getConnection(connectionURL);
			//Statement statement = conn.createStatement();
			String SelSql = "SELECT MAX(id) FROM offering";
			//ResultSet result = statement.executeQuery("SELECT MAX(id) FROM offering");
			ResultSet result = select(SelSql, null);
			result.next();
			int newId = 1 + result.getInt(1);
			String sql = "INSERT INTO offering VALUES (?, ?, ?)";
			String[] param = new String[3];
			param[0] = Integer.toString(newId);
			param[1] = course.getName();
			param[2] = daysTimesCsv;
			executeSQL(sql, param);
			/*(statement.executeUpdate("INSERT INTO offering VALUES (" + newId + ",'"
					+ course.getName() + "','" + daysTimesCsv + "')"); */
			return new Offering(newId, course, daysTimesCsv);
	}

	public static Offering find(int id) {
		Connection conn = null;

		try {
//			conn = DriverManager.getConnection(connectionURL);
//			Statement statement = conn.createStatement();
//			ResultSet result = statement.executeQuery("SELECT * FROM offering WHERE id =" + id
//					+ "");
			String sql = "SELECT * FROM offering WHERE id = ?";
			String[] param = new String[1];
			param[0] = Integer.toString(id);
			ResultSet result = select(sql, param);
			if (!result.next())
				return null;
			String courseName = result.getString("name");
			Course course = Course.find(courseName);
			String dateTime = result.getString("daysTimes");


			return new Offering(id, course, dateTime);
		} catch (Exception ex) {
			return null;
		}
	}

	public void update() throws Exception {
//		Connection conn = null;
//
//		try {
//			conn = DriverManager.getConnection(connectionURL);
//			Statement statement = conn.createStatement();
//
//			statement.executeUpdate("DELETE FROM offering WHERE id=" + id + "");
//			statement.executeUpdate("INSERT INTO offering VALUES(" + id + ",'" + course.getName()
//					+ "','" + daysTimes + "')");
//		} finally {
//			try {
//				conn.close();
//			} catch (Exception ignored) {
//			}
//		}
		String DelSql = "DELETE FROM offering WHERE id = ?";
		String[] DelParam = new String[1];
		DelParam[0] = Integer.toString(id);
		String InsSql = "INSERT INTO offering VALUES (?, ?, ?)";
		String[] InsParam = new String[3];
		InsParam[0] = Integer.toString(id);
		InsParam[1] = course.getName();
		InsParam[2] = daysTimes;
		executeSQL(DelSql, DelParam);
		executeSQL(InsSql,InsParam);
	}

	public Offering(int id, Course course, String daysTimesCsv) {
		this.id = id;
		this.course = course;
		this.daysTimes = daysTimesCsv;
	}

	public int getId() {
		return id;
	}

	public Course getCourse() {
		return course;
	}

	public String getDaysTimes() {
		return daysTimes;
	}

	public String toString() {
		return "Offering " + getId() + ": " + getCourse() + " meeting " + getDaysTimes();
	}
}
