import java.sql.*;

public class Course extends Base{
	private String name;
	private int credits;
	static String dbName="reggie";
	static String connectionURL = "jdbc:derby:" + dbName + ";create=true";
	private Connection conn;
	private PreparedStatement pstmt;
	private ResultSet rs;

	public static Course create(String name, int credits) throws Exception {
			Connection conn = null;

		//try {
			//conn = DriverManager.getConnection(connectionURL);
			//conn = getConn();
			//Statement statement = conn.createStatement();

			String DelSql = "DELETE FROM course WHERE name = ?";
			String InsSql = "INSERT INTO course VALUES (?, ?)";
			String[] InsParam = new String[2];
			InsParam[0] = name;
			InsParam[1] = Integer.toString(credits);
			String[] DelParam =  new String[1];
			executeSQL(DelSql, DelParam);
			executeSQL(InsSql, InsParam);
//			statement.executeUpdate("DELETE FROM course WHERE name = '" + name + "'");
//			statement.executeUpdate("INSERT INTO course VALUES ('" + name + "', " + credits
//					+ ")");
			return new Course(name, credits);
//		} finally {
//			try {
//				conn.close();
//			} catch (Exception ignored) {
//			}
//		}
	}

	public static Course find(String name) {
		Connection conn = null;

			/*conn = DriverManager.getConnection(connectionURL);
			Statement statement = conn.createStatement();*/
			//conn = getConn();
//			ResultSet result = statement.executeQuery("SELECT * FROM course WHERE name = '" + name
//					+ "'");
			String sql = "SELECT * FROM course WHERE name = ?";
			String[] param = new String[1];
			param[0] = name;
			ResultSet result = select(sql, param);
			int credits = -1;
			try {
				if (!result.next())
					return null;

				credits = result.getInt("Credits");

			}
			catch (SQLException e){
				e.printStackTrace();
			}
		return new Course(name, credits);
	}

	public void update() throws Exception {
		/*Connection conn = null;

		try {
			conn = DriverManager.getConnection(connectionURL);
			Statement statement = conn.createStatement();

			statement.executeUpdate("DELETE FROM course WHERE name = '" + name + "'");
			statement.executeUpdate("INSERT INTO course VALUES('" + name + "'," + credits + ")");
		} finally {
			try {
				conn.close();
			} catch (Exception ignored) {
			}
		}*/
		create(name, credits);
	}

	Course(String name, int credits) {
		this.name = name;
		this.credits = credits;
	}

	public int getCredits() {
		return credits;
	}

	public String getName() {
		return name;
	}
}
