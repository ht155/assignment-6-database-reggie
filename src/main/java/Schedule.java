import java.util.*;
import java.sql.*;

public class Schedule extends Base{
	String name;
	int credits = 0;
	static final int minCredits = 12;
	static final int maxCredits = 18;
	boolean overloadAuthorized = false;
	ArrayList<Offering> schedule = new ArrayList<>();


	static String dbName="reggie";
	static String connectionURL = "jdbc:derby:" + dbName + ";create=true";


	public static void deleteAll() throws Exception {
		Connection conn = null;


		String sql =  "DELETE FROM schedule";
		executeSQL(sql, null);
	}

	public static Schedule create(String name) throws Exception {
		Connection conn = null;



			String sql = "DELETE FROM schedule WHERE name = ?";
					//statement.executeUpdate("DELETE FROM schedule WHERE name = '" + name + "'");
			String[] params = new String[1];
			params[0] = name;
			executeSQL(sql, params);
			return new Schedule(name);
	}

	public static Schedule find(String name) {
		Connection conn = null;
		try {
			String sql  = "SELECT * FROM schedule WHERE name=?";
			String[] params = new String[1];
			params[0] = name;
			ResultSet result = select(sql, params);
			Schedule schedule = new Schedule(name);
			while (result.next()) {
				int offeringId = result.getInt("offeringId");
				Offering offering = Offering.find(offeringId);
				schedule.add(offering);
			}

			return schedule;
		} catch (Exception ex) {
			return null;
		} finally {
			try {
				conn.close();
			} catch (Exception ignored) {
			}
		}
	}

	public static Collection<Schedule> all() throws Exception {
		ArrayList<Schedule> result = new ArrayList<Schedule>();

		try {
			String sql = "SELECT DISTINCT name FROM schedule ORDER BY name";
			ResultSet results = select(sql, null);//statement.executeQuery("SELECT DISTINCT name FROM schedule ORDER BY name");

			while (results.next())
				result.add(Schedule.find(results.getString("name")));
		} finally {

		}

		return result;
	}

	public void update() throws Exception {
		Connection conn = null;


		conn = DriverManager.getConnection(connectionURL);
		Statement statement = conn.createStatement();
		String del = "DELETE FROM schedule WHERE name = ?";
		//statement.executeUpdate("DELETE FROM schedule WHERE name = '" + name + "'");
		String [] names = new String[1];
		names[0] = name;
		executeSQL(del, names);

		for (int i = 0; i < schedule.size(); i++) {
			Offering offering = (Offering) schedule.get(i);
				/*int resultCount = statement.executeUpdate("INSERT INTO schedule VALUES('" + name + "',"
						+ offering.getId() + ")");*/
			String ins = "INSERT INTO schedule VALUES(?, ?)";
			String[] param = new String[2];
			param[0] = name;
			param[1] = Integer.toString(offering.getId());
			executeSQL(ins, param);
		}
	}


	public Schedule(String name) {
		this.name = name;
	}

	public void add(Offering offering) {
		credits += offering.getCourse().getCredits();
		schedule.add(offering);
	}

	public void authorizeOverload(boolean authorized) {
		overloadAuthorized = authorized;
	}

	public List<String> analysis() {
		ArrayList<String> result = new ArrayList<String>();

		if (credits < minCredits)
			result.add("Too few credits");

		if (credits > maxCredits && !overloadAuthorized)
			result.add("Too many credits");

		checkDuplicateCourses(result);

		checkOverlap(result);

		return result;
	}

	public void checkDuplicateCourses(ArrayList<String> analysis) {
		HashSet<Course> courses = new HashSet<Course>();
		for (int i = 0; i < schedule.size(); i++) {
			Course course = ((Offering) schedule.get(i)).getCourse();
			if (courses.contains(course))
				analysis.add("Same course twice - " + course.getName());
			courses.add(course);
		}
	}

	public void checkOverlap(ArrayList<String> analysis) {
		HashSet<String> times = new HashSet<String>();

		for (Iterator<Offering> iterator = schedule.iterator(); iterator.hasNext();) {
			Offering offering = (Offering) iterator.next();
			String daysTimes = offering.getDaysTimes();
			StringTokenizer tokens = new StringTokenizer(daysTimes, ",");
			while (tokens.hasMoreTokens()) {
				String dayTime = tokens.nextToken();
				if (times.contains(dayTime))
					analysis.add("Course overlap - " + dayTime);
				times.add(dayTime);
			}
		}
	}

	public String toString() {
		return "Schedule " + name + ": " + schedule;
	}
}
